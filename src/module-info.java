module sergioTest3 {
    requires transitive javafx.graphics;
    requires javafx.controls;
    requires javafx.media;
    requires javafx.base;
    requires javafx.web;
    requires javafx.swing;
    requires javafx.fxml;
    
    exports question3;
    opens question3 to javafx.graphics;
}