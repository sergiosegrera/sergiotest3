package question1;

public class Foo {
	public static int foo(int n) {
		if (n <= 0) {
			return Math.abs(n);
		}
		
		int x = foo(n-2);
		int y = foo(n-2);
		int z = foo(n-1);
		return x + y + z;
	}

	public static void main(String[] args) {
		System.out.println(foo(4));
	}

}
