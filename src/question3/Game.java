package question3;

import javafx.application.Application;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

public class Game extends Application {
	private GameLogic gameLogic = new GameLogic(500);
	
	public void start (Stage stage) {
		Group root = new Group();
		
		// Headers
		TextField output = new TextField("Dice Roll Game!");
		TextField money = new TextField(Integer.toString(gameLogic.getMoney()));
		HBox header = new HBox();
		header.getChildren().addAll(output, money);	
		
		// Bet input
		TextField betInput = new TextField("Enter bet amount..");
		HBox input = new HBox();
		input.getChildren().add(betInput);
		
		// small or large selection buttons
		Button smallButton = new Button("small");
		Button largeButton = new Button("large");
		HBox buttons = new HBox();
		buttons.getChildren().addAll(smallButton, largeButton);
		
		// Add everything
		VBox content = new VBox();
		content.getChildren().addAll(header, input, buttons);
		root.getChildren().add(content);
		
		Scene scene = new Scene(root, 300, 300);
		
		// Attach event handlers
		GameEngine smallHandler = new GameEngine("small", gameLogic, money, betInput, output);
		smallButton.setOnAction(smallHandler);
		
		GameEngine largeHandler = new GameEngine("large", gameLogic, money, betInput, output);
		smallButton.setOnAction(largeHandler);
	
		
		stage.setTitle("DiceRoll Game");
		stage.setScene(scene);
		stage.show();
	}

	public static void main(String[] args) {
		launch(args);
	}

}
