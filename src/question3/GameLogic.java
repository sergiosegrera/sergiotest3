package question3;

import java.util.Random;

public class GameLogic {
	private int money;
	private Random rand;
	
	GameLogic(int money) {
		this.money = money;
		this.rand = new Random();
	}
	
	public String play(boolean bet, int betAmount) {
		// If bet is higher than the money the user has return error message
		if (betAmount > this.money) {
			return "Not enough money to place bet";
		}
		
		// Generates roll
		int roll = this.rand.nextInt(6) + 1;
		
		// If the bet guess is right double the money
		if ((roll > 3) == bet) {
			this.money += betAmount * 2;
			return "You won";
		}
		
		// Remove betAmount from money
		this.money -= betAmount;
		return "You Lost";
	}
	
	// Returns money to update the ui
	public int getMoney() {
		return this.money;
	}
}
