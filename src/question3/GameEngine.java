package question3;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.TextField;


// GameEngine handles the button events and updates the ui
public class GameEngine implements EventHandler<ActionEvent> {
	private String event;
	private GameLogic gameLogic;
	private TextField money;
	private TextField input;
	private TextField output;
	
	public GameEngine(String event, GameLogic gamelogic, TextField money, TextField input, TextField ouput) {
		this.event = event;
		this.gameLogic = gameLogic;
		this.money = money;
		this.input = input;
		this.output = output;
	}
	
	public void handle(ActionEvent event) {
		if (this.event.equals("small")) {
			// Sets output text
			this.output.setText(this.gameLogic.play(false, Integer.parseInt(this.input.getText())));
			// Sets new money total
			this.money.setText("" + this.gameLogic.getMoney());
		} else if (this.event.equals("large")) {
			this.output.setText(this.gameLogic.play(true, Integer.parseInt(this.input.getText())));
			this.money.setText("" + this.gameLogic.getMoney());
		}
	}

}
