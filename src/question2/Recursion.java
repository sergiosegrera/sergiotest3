package question2;

public class Recursion {
	
	public static int recursiveCount(int[] numbers, int n) {
		// if n is longer than numbers.length stop recursion
		if (numbers.length < n + 1) {
			return 0;
		}
		// if it matches the condition return 1 to add to count and do recursion
		if (numbers[n] < 10 && n % 2 == 0) {
			return 1 + recursiveCount(numbers, n + 1);
		// if it doesn't match don't count it and do recursion
		}
		return recursiveCount(numbers, n + 1);
	}

	public static void main(String[] args) {
		int numbers[] = {3, 4, 5, 6, 8, 10, 20};
		System.out.println(recursiveCount(numbers, 0));
	}

}